import React, { Component } from 'react';
import {BrowserRouter as Router,Switch,Route,Redirect} from 'react-router-dom';
import './App.css';
import Home from './components/Home';
import Video from './components/Video';
import Account from './components/Account';
import Menu from './components/Menu';
import MainPage from './components/MainPage';
import Auth from './components/Auth';
import Welcome from './components/Welcome';

export default class App extends Component {
  constructor(){
    super();
    this.state={
      loggedIn:true,
      usename:'',
      password:'',
      data:[
        {
          id:1,
          image:'https://9tailedkitsune.com/wp-content/uploads/2019/12/thestoryofSaiunkoku-800x445.png',
          title:'Best Historical Romance Anime',
          description:'It wouldn’t be the best historical romance anime if I could not include some Chinese anime here because they are currently the best option when it comes to this. I am not sure why, but they exactly know what an aching heart needs – a bishounen guy x cute girl concept.',
          update:'Last update 3 months ago',
        },
        {
          id:2,
          image:'https://9tailedkitsune.com/wp-content/uploads/2019/12/romexjuliet-1024x670.png',
          title:'Romeo x Juliet',
          description:'Plot Summary:  The story is set on a giant island floating in the sky, with the capital of Neo Verona heavily divided between nobility and commoners. Juliet Capulet is the only survivor of Montague’s assault which resulted in the slaughter of her entire family and complete control of the kingdom by the Montague family.',
          update:'Last update 3 months ago',
        },
        {
          id:3,
          image:'https://9tailedkitsune.com/wp-content/uploads/2019/12/mengqishishen.png',
          title:'Meng Qi Shi Shen',
          description:'lot Summary: Ye Jiayao has been dreaming of opening the best restaurant in Huai Song for quite some time – only to find herself back in time in the body of Ye Jinxuan in medieval China. What is worse – the bad luck follows and she ends up in the imperial era with zero skill in fighting.',
          update:'Last update 3 months ago',
        },
        {
          id:4,
          image:'https://www.gojinshi.com/wp-content/uploads/2020/02/Best-Reverse-Harem-Anime-3.jpg',
          title:'Saiunkoku Monogatari',
          description:'Plot Summary:  Despite being born to a noble family, Shuurei Kou is barely managing to get enough money to support her family due to the generally low income of her father.',
          update:'Last update 3 months ago',
        },
        {
          id:5,
          image:'https://9tailedkitsune.com/wp-content/uploads/2019/12/Bez-n%C3%A1zvu-1-1024x730.png',
          title:'Fushigi Yuugi',
          description:'Plot Summary:  Miaka Yuki used to be an ordinary middle schooler until she discovered a mysterious book called The Universe of the Four Gods at the library. While she read the book she got transported into the novel’s universe in ancient China along with her friend Yui.',
          update:'Last update 3 months ago',
        },
        {
          id:6,
          image:'https://9tailedkitsune.com/wp-content/uploads/2019/12/shengshizhuangniang-1024x576.jpg',
          title:'Sheng Shi Zhuang Niang',
          description:'Plot Summary: After suffering a car accident and becoming “a vegetable” in real life, the consciousness of a  beauty blogger is trapped in a VR game where she has to complete tasks related to make up.',
          update:'Last update 3 months ago',
        },
      ],
    }
    this.handleLoggin=this.handleLoggin.bind(this);
  }
  handleLoggin=()=>{
    if(this.state.usename!==''&&this.state.password!==''){
      this.setState({
        loggedIn:false,
      })
    } 
    this.setState({
      usename:'',
      password:'',
    })
  }

  handleChange=(e)=>{
    this.setState({
      [e.target.name]:e.target.value,
    })
  }

  render() {
    return (
      <div className="App">
        <Router>
          <Menu />
          <Switch>
            
            <Route path='/' exact component={MainPage}/>
            <Route path='/home'  render={(props)=><Home {...props} data={this.state.data}/>}/>
            <Route path='/video' component={Video}/>
            <Route path='/account' component={Account}/>
            <Route path='/auth' render={()=><Auth onChanged={this.handleChange} loggedIn={this.handleLoggin}/>}/>
            <Route exact path="/welcome">
              {this.state.loggedIn? <Redirect to="/auth" /> : <Welcome />}
            </Route>
          </Switch>
        </Router>
      </div>
    )
  }
}
