import React from 'react';
import {Card} from 'react-bootstrap';
import '../App.css';

export default function Home({data}) {
    
    let datas = data.map((d)=>(
        <div className="col-md-4"  key={d.id}>
            <Card className="card">
                <Card.Img variant="top" src={d.image} />
                <Card.Body>
                <Card.Title>{d.title}</Card.Title>
                <Card.Text>
                    {d.description}
                </Card.Text>
                </Card.Body>
                <Card.Footer>
                    <h6>{d.update}</h6>
                </Card.Footer>
            </Card>
        </div>))
    return (
        <div className="container">
            <div className="row">
            {datas}
            </div>
        </div>
    )
}
