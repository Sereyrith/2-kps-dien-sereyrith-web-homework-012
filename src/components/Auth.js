import React from 'react'
import {Link} from 'react-router-dom';
import '../App.css';

export default function Auth(props) {
  return (
    <div className="container" style={{marginTop:'10px'}}>
      <input type="text" name="usename" onChange={(e)=>props.onChanged(e)} value={props.usename}/>
      <input type="password" name="password" onChange={(e)=>props.onChanged(e)} value={props.password}/>
      <Link to={`/welcome`}><button onClick={()=>props.loggedIn()}>Submit</button></Link>
    </div>
  )
}
