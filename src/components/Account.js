import {
    BrowserRouter as Router,
    Link,
    useLocation
  } from "react-router-dom";
import React from 'react'

export default function Account() {
    let query = useQuery();

  return (
    <div className="container">
      <h2>Accounts</h2>

      <ul>
          <li>
            <Link to="/account?name=netflix">Netflix</Link>
          </li>
          <li>
            <Link to="/account?name=zillow-group">Zillow Group</Link>
          </li>
          <li>
            <Link to="/account?name=yahoo">Yahoo</Link>
          </li>
          <li>
            <Link to="/account?name=modus-create">Modus Create</Link>
          </li>
        </ul>

        <Child name={query.get("name")} />
    </div>
  );
}
function useQuery() {
    return new URLSearchParams(useLocation().search);
  }
  function Child({ name }) {
    return (
      <div>
        {name ? (
          <h3>
            The <code>name</code> in the query string is &quot;{name}
            &quot;
          </h3>
        ) : (
          <h3>There is no name in the query string</h3>
        )}
      </div>
    );
  }
  