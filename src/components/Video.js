import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useRouteMatch,
    useParams
  } from "react-router-dom";
import React from 'react'

export default function Video() {
    let match = useRouteMatch();
    return (
        <Router>
      <div className="container">
        <ul>
          <li>
            <Link to={`${match.path}/Animation`}>Animation</Link>
          </li>
          <li>
            <Link to={`${match.path}/Movie`}>Movie</Link>
          </li>
        </ul>

        <Switch>
          <Route path={`${match.path}/Animation`}><Animation /></Route>
          <Route path={`${match.path}/Movie`}><Movie /></Route>
        </Switch>
      </div>
    </Router>
    );
}

function Animation() {
    let match = useRouteMatch();
    return (
      <div>
        <h2>Animation Category</h2>
  
        <ul>
          <li>
            <Link to={`${match.url}/Action`}>Action</Link>
          </li>
          <li>
            <Link to={`${match.url}/Romance`}>Romance</Link>
          </li>
          <li>
            <Link to={`${match.url}/Comedy`}>Comedy</Link>
          </li>
        </ul>
  
        <Switch>
          <Route path={`${match.path}/:topicId`}>
            <Topic />
          </Route>
          <Route path={match.path}>
            <h3>Please select a topic.</h3>
          </Route>
        </Switch>
      </div>
    );
  }
  
  function Movie() {
    let match = useRouteMatch();
    return (
      <div>
        <h2>Movie Category</h2>
  
        <ul>
          <li>
            <Link to={`${match.url}/Adventure`}>Adventure</Link>
          </li>
          <li>
            <Link to={`${match.url}/Comedy`}>Comedy</Link>
          </li>
          <li>
            <Link to={`${match.url}/Crime`}>Crime</Link>
          </li>
          <li>
            <Link to={`${match.url}/Documentary`}>Documentary</Link>
          </li>
        </ul>
  
        <Switch>
          <Route path={`${match.path}/:topicId`}>
            <Topic />
          </Route>
          <Route path={match.path}>
            <h3>Please select a topic.</h3>
          </Route>
        </Switch>
      </div>
    );
  }
  
  function Topic() {
    let { topicId } = useParams();
    return <h3>{topicId}</h3>;
  }